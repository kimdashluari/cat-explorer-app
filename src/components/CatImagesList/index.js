import React from 'react';
import { Link } from 'react-router-dom';

import {
  Alert,
  Button,
  Card,
  Col,
  Row,
  Spinner
} from 'react-bootstrap';

import {
  RESULTS_LIMIT,
  fetchCatImagesByBreed
} from '../../services/catImagesApi';

class CatImagesList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cats: [],
      isFetching: false,
      error: null
    };
  }

  componentDidMount() {
    this.fetchCats();
  }

  componentDidUpdate(prevProps) {
    if (this.props.selectedBreed !== prevProps.selectedBreed) {
      this.fetchCats();
    }

    if (this.props.currentPage !== prevProps.currentPage) {
      const replaceCatImagesList = false;
      this.fetchCats(replaceCatImagesList);
    }
  }

  render() {

    if (this.state.error) {
      return (
        <Alert variant="danger">
          {this.state.error}
        </Alert>
      )
    }

    return (
      <section className="cat-images-list">
        {this.getSpinner()}
        <Row className="my-4">
          {this.getResults()}
        </Row>
      </section>
    );
  }

  /**
   * Display a spinner based on the `isFetching` state.
  */
  getSpinner() {
    if (!this.state.isFetching) return null;

    return (
      <div className="my-4 text-center">
        <Spinner animation="border" variant="primary" role="status">
          <span className="sr-only">Fetching cats...</span>
        </Spinner>
      </div>
    );
  }

  /**
   * Displays results based on the count of results.
  */
  getResults() {
    const { cats } = this.state;

    /**
     * Displays empty results message.
     */
    if (!cats.length) {
      return (
        <Col>
          <Alert variant="info">
            No cats available!
          </Alert>
        </Col>
      );
    }

    /**
     * Map array of `cats` into bootstrap columns.
     */
    return cats.map(({ id, url }, index) => {
      return (
        <Col key={index} sm="6" md="4" lg="3" className="mb-4">
          <Card className="h-100">
            <Card.Img variant="top" src={url}>
            </Card.Img>
            <Card.Body className="d-flex flex-row align-items-end">
              <Link to={`/cats/${id}`} className="d-block w-100">
                <Button variant="primary" className="w-100 text-uppercase">
                  View details
                </Button>
              </Link>
            </Card.Body>
          </Card>
        </Col>
      );
    });
  }

  /**
   * Handles fetching of cats by breed.
   */
  async fetchCats(replaceCatImagesList = true) {
    const {
      selectedBreed,
      currentPage,
      onBeforeLoadCats,
      onLoadCats
    } = this.props;

    /**
     * Sets `cats` to empty array and `isFetching` to false
     * if `selectedBreed` is empty.
    */
    if (!selectedBreed) {
      return this.setState(
        () => ({
          cats: [],
          isFetching: false
        })
      );
    }

    /**
     * Action hook before start of fetching.
    */
    onBeforeLoadCats(replaceCatImagesList);

    /**
     * - Determines if the cats list will be replaced or
     *   be appended with new ones.
     * - Replacing is for fetching cats list by breed.
     * - Appending is for fetching paginated cats list by breed.
    */
    if (replaceCatImagesList) {
      this.setState(
        () => ({
          cats: [],
          isFetching: true
        })
      );
    } else {
      this.setState(
        () => ({
          isFetching: true
        })
      );
    }

    try {
      /**
       * Fetch paginated cat images by breed ID.
       */
      let newCats = await fetchCatImagesByBreed(selectedBreed, currentPage);

      this.setState(
        (state) => {
  
          /**
           * - Determines if the cats list will be replaced or
           *   be appended with new ones.
           * - Replacing is for fetching cats list by breed.
           * - Appending is for fetching paginated cats list by breed.
          */
          let cats = newCats;
          if (!replaceCatImagesList) {
            cats = [
              ...state.cats,
              ...newCats
            ];
          }
  
          return {
            cats,
            isFetching: false
          }
        }
      );
  
      /**
       * - Determines if there are still loadable
       * results.
       * - @NOTE: I didn't use the Pagination-Count header from
       * thecatapi.com because I find this method much easier.
       * Checking the last result if the count of `newCats` is not less than the
       * RESULTS_LIMIT which is 8.
       */
      const canLoadMore = !(newCats.length < RESULTS_LIMIT);

      /**
       * Action hook after end of fetching.
      */
      onLoadCats(replaceCatImagesList, canLoadMore);
    } catch(error) {
      /**
       * Updates `error` in state for API call failure.
      */
      this.setState(
        () => ({
          error: 'Apologies but we could not load new cats for you at this time! Miau!'
        })
      );
    }
  }
}

export default CatImagesList;