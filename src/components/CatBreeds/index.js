import React from 'react';

import {
  Alert,
  Col,
  Form,
  Row,
  Spinner
} from 'react-bootstrap';

import {
  fetchCatBreeds
} from '../../services/catImagesApi';

class CatBreeds extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      breeds: [],
      isFetching: true,
      error: null
    }
  }

  componentDidMount() {
    this.fetchCatBreeds();
  }

  render() {
    const {
      selectedBreed = '',
      onSelectBreed
    } = this.props;

    if (this.state.error) {
      return (
        <Alert variant="danger">
          {this.state.error}
        </Alert>
      );
    }

    return (
      <Row>
        <Col xs="3">
          <Form.Group controlId="catBreeds">
            <Form.Label>Breed</Form.Label>
            <div className="d-flex align-items-center">
              <Form.Control
                as="select"
                custom
                value={selectedBreed}
                disabled={this.state.isFetching}
                onChange={e => {
                  onSelectBreed(e.target.value);
                }}>
                {this.getOptions()}
              </Form.Control>
              {this.getSpinner()}
            </div>
          </Form.Group>
        </Col>
      </Row>
    );
  }

  /**
   * Handles asynchronous request for cat breeds.
   * Runs the `onLoadBreeds` prop after fetching the cat breeds.
   */
  async fetchCatBreeds() {
    try {
      const breeds = await fetchCatBreeds();
      
      this.setState(
        () => ({
          breeds,
          isFetching: false
        })
      );

    this.props.onLoadBreeds(true);
    } catch(error) {
      this.setState(
        state => ({
          error: 'Apologies! We could not load cat breeds selection for you at this time!'
        })
      );
    }
  }

  /**
   * Generates list of `<option></option>` elements
   * based on the list of cat breeds fetched from thecatapi.com API.
  */
  getOptions() {
    const emptyOption = <option key="" value="">-- Select Breed --</option>;

    const options = this.state.breeds.map(({
      id,
      name
    }) => {
      return (
        <option
          key={id}
          value={id}>
          {name}
        </option>
      )
    });

    return [
      emptyOption,
      ...options
    ];
  }

  /**
   * Display a spinner based on the `isFetching` state.
  */
  getSpinner() {
    if (!this.state.isFetching) return null;

    return (
      <div>
        <Spinner animation="border" size="sm" role="status" variant="success" className="ml-2">
          <span className="sr-only">Loading...</span>
        </Spinner>
      </div>
    );
  }
}

export default CatBreeds;