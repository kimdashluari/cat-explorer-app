import React from 'react';

import { withRouter } from 'react-router';

import { INITIAL_PAGE } from '../../services/catImagesApi';

import CatBreeds from '../../components/CatBreeds';
import CatImagesList from '../../components/CatImagesList';
import LoadMore from '../../components/LoadMore';

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      breeds: {
        selectedBreed: '',
        isLoaded: false
      },
      cats: {
        currentPage: INITIAL_PAGE,
        isLoadingMore: false,
        canLoadMore: false
      }
    }
  }

  componentDidMount() {
    this.fetchCatsByUrlSearchParams();
  }

  render() {
    return (
      <>
        <h1 className="mb-4">
          Cats Explorer
        </h1>
        <CatBreeds
          selectedBreed={this.state.breeds.selectedBreed}
          onSelectBreed={this.handleCatBreedSelect}
          onLoadBreeds={this.handleCatBreedsLoad} />
        <CatImagesList
          selectedBreed={this.state.breeds.selectedBreed}
          currentPage={this.state.cats.currentPage}
          onBeforeLoadCats={this.handleBeforeCatsFetchByBreed}
          onLoadCats={this.handleCatsFetchByBreed} />
        <LoadMore
          hidden={this.state.cats.canLoadMore}
          isLoading={this.state.cats.isLoadingMore}
          onClick={this.handleLoadMoreCats} />
      </>
    );
  }

  /**
   * Detects query parameter `breed` from URL and force
   * a state change for `state.breeds.selectedBreed` that will trigger
   * a fetch of cats images via `CatImagesList` component
   */
  fetchCatsByUrlSearchParams() {
    const urlSearchParams = new URLSearchParams(this.props.location.search);

    if (urlSearchParams.has('breed')) {
      const selectedBreed = urlSearchParams.get('breed');

      this.setState(
        state => ({
          breeds: {
            ...state.breeds,
            ...{
              selectedBreed
            }
          }
        })
      );
    }
  }

  /**
   * On cat breed select, save the selected value into
   * URL query parameters' `breed` key.
   */
  saveCatBreedToUrlSearcParams(selectedBreed) {
    const urlSearchParams = new URLSearchParams(this.props.location.search);
    urlSearchParams.set('breed', selectedBreed);
    
    this.props.history.push({
      pathname: '/',
      search: `?${urlSearchParams.toString()}`
    });
  }

  /**
   * Called when fetching the list of cat breeds is done.
   */
  handleCatBreedsLoad = (isLoaded = false) => {
    this.setState(
      state => ({
        breeds: {
          ...state.breeds,
          ...{
            isLoaded
          }
        }
      })
    );
  }

  /**
   * Called before fetching the list of cat images by breed.
   */
  handleBeforeCatsFetchByBreed = (replaceCatImagesList = true) => {
    this.setState(
      state => ({
        cats: {
          ...state.cats,
          ...{
            isLoadingMore: !replaceCatImagesList
          }
        }
      })
    );
  }

  /**
   * Called after fetching the list of cat images by breed.
   */
  handleCatsFetchByBreed = (canLoadMore = false) => {
    this.setState(
      state => ({
        cats: {
          ...state.cats,
          ...{
            canLoadMore
          }
        }
      })
    );
  }

  /**
   * Called after selecting cat breed.
   */
  handleCatBreedSelect = (selectedBreed = '') => {
    this.setState(
      state => ({
        breeds: {
          ...state.breeds,
          ...{
            selectedBreed
          }
        },
        cats: {
          ...state.cats,
          ...{
            canLoadMore: false
          }
        }
      })
    );

    this.saveCatBreedToUrlSearcParams(selectedBreed);
  }

  /**
   * Called after clicking load more button.
   */
  handleLoadMoreCats = () => {
    this.setState(
      state => ({
        cats: {
          ...state.cats,
          ...{
            currentPage: state.cats.currentPage + 1,
            isLoadingMore: true
          }
        }
      })
    );
  }
}

export default withRouter(Home);