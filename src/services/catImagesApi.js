import axios from './axios';

// Registered API key from thecatapi.com
export const API_KEY = '3b4f5216-8571-4cb6-a3a4-c9949fedcc15';

// API Version.
export const API_VERSION = 'v1';

// API base URL.
export const API_BASE_URL = `https://api.thecatapi.com/${API_VERSION}`;

/**
 * Cats search result initial page start.
 * @NOTE: thecatapi.com's correct page start is 0 instead of 1.
 */
export const INITIAL_PAGE = 0;

// Cats search result limit.
export const RESULTS_LIMIT = 8;

/**
 * Fetch list of cat breeds from the API.
 */
export const fetchCatBreeds = async () => {
  const { data } = await axios.get(`${API_BASE_URL}/breeds`);
  return data;
};

/**
 * Fetch paginated cats list filtered by breed ID.
 */
export const fetchCatImagesByBreed = async (
  breed_id = '',
  page = INITIAL_PAGE,
  limit = RESULTS_LIMIT
) => {
  const requestOptions = {
    params: {
      size: 'med',
      order: 'ASC',
      limit,
      page,
      breed_id
    }
  };

  const { data } = await axios.get(`${API_BASE_URL}/images/search`, requestOptions);
  return data;
};

/**
 * Fetch single cat by ID.
 */
export const fetchCatImageById = async (
  id = null
) => {
  const { data } = await axios.get(`${API_BASE_URL}/images/${id}`);
  return data;
}