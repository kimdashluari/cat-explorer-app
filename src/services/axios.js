import axios from 'axios';
import { API_KEY } from './catImagesApi';

/**
 * This will make sure that the modules importing this module
 * will have instance of `axios` module attached with the correct 
 * value for the `x-api-key` request header used for thecatapi.com API.
*/
axios.interceptors.request.use(config => {
  config.headers['x-api-key'] = API_KEY;
  return config;
});

export default axios;