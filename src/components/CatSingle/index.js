import React from 'react';

import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';

import {
  Alert,
  Button,
  Card,
  Spinner
} from 'react-bootstrap';

import { fetchCatImageById } from '../../services/catImagesApi';

class CatSingle extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cat: null,
      isFetching: true,
      error: null
    }
  }

  componentDidMount() {
    this.fetchCat();
  }

  /**
   * Handles asynchronous fetching of single cat image
   * based on `catId` parsed from URL.
   */
  async fetchCat() {
    const { catId } = this.props.match.params;
    
    try {
      const cat = await fetchCatImageById(catId);
    
      this.setState(
        () => ({
          cat,
          isFetching: false
        })
      );
    } catch(error) {
      this.setState(
        state => ({
          error: `Apologies but we could not load this cat with ID of ${catId} for you at this time! Miau!`
        })
      );
    }
  }

  render() {
    let spinner;
    let result;
    const cat = this.state.cat;

    if (this.state.isFetching) {
      spinner = (
        <div className="my-4 text-center">
          <Spinner animation="border" variant="primary" role="status">
            <span className="sr-only">Fetching cats...</span>
          </Spinner>
        </div>
      );
    }

    if (this.state.error) {
      return (
        <Alert variant="danger">
          {this.state.error}
        </Alert>
      )
    }

    if (cat) {
      const {
        url: catImageUrl,
        breeds: [
          {
            id: breedId,
            name: breedName,
            origin: breedOrigin,
            description: breedDescription,
            temperament: breedTemperament
          }
        ]
      } = this.state.cat;

      result = (
        <Card>
          <Card.Header>
            <Link to={{
              pathname: '/',
              search: `?breed=${breedId}`
            }}>
              <Button variant="primary">
                Back
              </Button>
            </Link>
          </Card.Header>
          <Card.Img src={catImageUrl} />
          <Card.Body>
            <Card.Title as="h4">{breedName}</Card.Title>
            <h5>Origin: {breedOrigin}</h5>
            <h6>{breedTemperament}</h6>
            <Card.Text>
              {breedDescription}
            </Card.Text>
          </Card.Body>
        </Card>
      );
    }

    return (
      <article className="single-cat">
        {spinner}
        {result}
      </article>
    );
  }
}

export default withRouter(CatSingle);