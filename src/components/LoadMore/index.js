import {
  Button,
  Col,
  Row,
  Spinner
} from 'react-bootstrap';

/**
 * Pure component solely for triggering a load more action
 * for cat images.
 */
const LoadMore = ({
  hidden = true,
  isLoading = false,
  onClick = () => {}
}) => {
  if (!hidden) return null;

  let spinner;
  const text = isLoading ? 'Loading' : 'Load More';

  if (isLoading) {
    spinner = <Spinner animation="border" size="sm" className="ml-1"/>
  }

  return (
    <Row>
      <Col sm="12" className="text-center">
        <Button
          variant="success"
          size="lg"
          onClick={onClick}
          className="text-uppercase">
          {text}{spinner}
        </Button>
      </Col>
    </Row>
  );
};

export default LoadMore;