import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

const NotFound = () => {
  return (
    <div className="d-flex flex-row justify-content-center align-items-center h-100">
      <header className="text-center">
        <h1>Error 404</h1>
        <p>Page not found!</p>
        <Link to="/">
          <Button variant="primary">Back to Home</Button>
        </Link>
      </header>
    </div>
  );
}

export default NotFound;