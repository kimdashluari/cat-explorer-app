import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import {
  Container
} from 'react-bootstrap';

import Home from './components/Home';
import CatSingle from './components/CatSingle';
import NotFound from './components/NotFound';

const App = () => {
  return (
    <Router>
      <Container className="p-4 py-md-5">
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/cats/:catId">
            <CatSingle />
          </Route>
          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </Container>
    </Router>
  );
};

export default App;